# PHPMailer Yii extension

## Config

```php
<?php
    'components' => [
        'mailer' => [
            'class' => 'application.extensions.yii-phpmailer.YiiPHPMailerInit',
            'charset' => 'UTF-8',
            'driver' => 'smtp',
            'secure' => 'ssl',
            'host' => 'smtp.gmail.com',
            'port' => 465,
            'auth' => true,
            'login' => 'youremail@gmail.com',
            'password' => '',
            'email_from' => '',
            'email_from_name' => '',
        ],
    ],
```

## Example

```php
Yii::app()->mailer->send(['test1@mail.ru' => 'NameTest1', 'test2@mail.ru' => 'NameTest2'], 'subject', 'body')
```
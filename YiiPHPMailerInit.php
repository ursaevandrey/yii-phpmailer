<?php

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Class YiiPHPMailerInit
 */
class YiiPHPMailerInit extends CApplicationComponent{

    const DRIVER_DEV = 'dev';
    const DRIVER_MAIL = 'mail';
    const DRIVER_SENDMAIL = 'sendmail';
    const DRIVER_SMTP = 'smtp';

    /**
     * @var PHPMailer
     */
    public $mailer;

    public $exceptions = false;
    public $driver;
    public $charset;
    public $host;
    public $port = 25;
    public $auth;
    public $login;
    public $password;
    public $secure = '';
    public $email_from;
    public $email_from_name;

    public function init(){
        parent::init();
        $this->mailer = new PHPMailer($this->exceptions);

        $this->_setSetting([
            'Mailer' => $this->driver,
            'CharSet' => $this->charset,
            'Host' => $this->host,
            'Port' => $this->port,
            'SMTPAuth' => $this->auth,
            'Username' => $this->login,
            'Password' => $this->password,
            'SMTPSecure' => $this->secure,
            'From' => $this->email_from != '' ? $this->email_from : $this->login,
            'FromName' => $this->email_from_name,
        ]);
    }

    /**
     * Sending email
     *
     * @param array $email_to [ [email, name], [email, name] ]
     * @param string $subject
     * @param string $body
     * @param array $options ['html' = bool, 'alt_body' = string, 'file' = ['path' = string, 'name' = string]]
     * @return array [email] => send bool
     */
    public function send($email_to = [], $subject = '', $body = '', $options = []){
        if($this->driver == self::DRIVER_DEV){
            return array('dev' => true);
        }

        $this->_setSetting($options);

        if(isset($options['html'])){
            $is_html = (bool) $options['html'];
            $this->mailer->IsHTML($is_html);
            $this->mailer->AltBody = ($is_html && isset($options['alt_body'])) ? $options['alt_body'] : '';
        }
        if(isset($options['file'])){
            $this->mailer->AddAttachment($options['file']['path'], $options['file']['name']);
        }

        $this->mailer->Subject = $subject;
        $this->mailer->Body = $body;

        $result = [];
        foreach($email_to as $email => $name){
            if(!is_string($email)){
                $email = $name;
            }elseif(!is_string($name)){
                $name = $email;
            }

            $this->mailer->AddAddress($email, $name);
            $result[$email] = $this->_validateEmail($email) ? $this->mailer->Send() : false;
            $this->mailer->ClearAddresses();
        }

        if($this->mailer->Mailer == self::DRIVER_SMTP){
            $this->mailer->SmtpClose();
        }

        return $result;
    }

    /**
     * @param array $settings
     */
    private function _setSetting($settings){
        foreach($settings as $name => $value){
            if(!isset($this->mailer->$name)){
                continue;
            }

            $this->mailer->$name = $value;
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    private function _validateEmail($email){
        return (bool) preg_match('/^[a-zA-Z0-9\.\_\-]+@[a-zA-Z0-9\_\-]+\.[a-zA-Z]+$/', $email);
    }

}